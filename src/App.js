import Navigation from "./components/navigation_bar/navigation";
import Notification from "./components/notification_bar/notification";
import Header from "./components/header/header";
import Body from "./components/body/body";
import "./App.css";

function App() {
  return (
    <div className="App">
      <Notification />
      <Navigation />
      <Header />
      <br />
      <Body />
    </div>
  );
}

export default App;
