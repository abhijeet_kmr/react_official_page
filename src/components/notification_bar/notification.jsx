import "./notification.css";

const notification = (props) => {
  return (
    <div className="notification">
      <span className="span-ukraine">Support Ukraine 🇺🇦 </span>
      <a href="https://opensource.fb.com/support-ukraine" target="_blank">
        <span className="span-details">
          Help Provide Humanitarian Aid to Ukraine.
        </span>
      </a>
    </div>
  );
};
export default notification;
