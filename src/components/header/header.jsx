import "./header.css";

const header = (props) => {
  return (
    <div className="container">
      <h2>React</h2>
      <p>A JavaScript library for building user interfaces</p>

      <div className="buttons">
        <button className="btn-get-started">Get Started</button>
        <a href="/tutorial/tutorial.html">Take the Tutorial&gt;</a>
      </div>
    </div>
  );
};

export default header;
